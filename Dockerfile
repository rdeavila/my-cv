FROM nginx:1.19-alpine
COPY public /usr/share/nginx/html
COPY conf/default.conf /etc/nginx/conf.d/default.conf
COPY local-certs/lvh_me.pem /etc/nginx/ssl/lvh_me.pem
COPY local-certs/lvh_me_key.pem /etc/nginx/ssl/lvh_me_key.pem