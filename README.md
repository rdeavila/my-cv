# My CV On Kubernetes

Developed using

* Windows 10 Pro
* Ubuntu 20.04 LTS @ WSL 2
* Docker Desktop, with WSL 2 Base Image and Kubernetes single-node cluster

## Adding the root certificate on Windows

Download the [root certificate][cacert] on your computer, and make windows trust
this certificate.

* open the Windows Powershell (Admin)
* Run `Import-Certificate -FilePath "rootCA.pem" -CertStoreLocation cert:\CurrentUser\Root`
* Click "Yes" in the next window. They are requesting your authorization to add
  the root certificate to the windows certificate chain

## Compiling the local Docker image, and making the k8s deployment

Using the Windows Powershell:

* clone this repo
* compile the Docker image locally
* do the deployment using this local image on your local cluster

```bash
git clone git clone https://rdeavila@bitbucket.org/rdeavila/k8s-cv.git
cd k8s-cv
docker build -t rda-cv:latest .
kubectl apply -f deployment.yaml
```

After the deploy, the CV website will be available on
[https://rda.lvh.me][rda-lvh-me]

This same image is available on [Docker Hub][docker-hub]. To use this, edit the
`deployment.yaml` file and replace

* `image: "rda-cv:latest"` by `image: "rdeavila/cv"`
* `imagePullPolicy: "Never"` by `imagePullPolicy: "Always"`.

[cacert]: local-certs/root_ca.pem
[rda-lvh-me]: https://rda.lvh.me
[docker-hub]: https://hub.docker.com/r/rdeavila/cv